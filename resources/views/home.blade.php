@include('include.header')

<div style="padding:10vh 0px 0px 10vh" class="form-group">

<div class="row">
    <div class="col-sm-6">
    @if(session('info'))
    <div class="alert alert-success">
        {{session('info')}}
    </div>
    @endif    
    </div>
</div>

<table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @if(count($articles) > 0)
            @foreach($articles->all() as $article)


            <tr>
                <td>{{ $article->id }}</td>
                <td>{{ $article->title }}</td>
                <td>{{ $article->description }}</td>
                <td>
                <a href='{{ url("/read/$article->id")}}' class="label btn-primary">Read</a> |
                <a href='{{ url("/update/$article->id")}}' class="label btn-success">Update</a> |
                <a href='{{ url("/delete/$article->id")}}' class="label btn-danger">Delete</a>
                </td>
            </tr>
            @endforeach
        @endif 
        </tbody>
    </table>
</div>

</body>

</html>