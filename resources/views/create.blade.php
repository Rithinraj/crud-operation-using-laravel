@include('include.header')


<div style="padding:10vh 0px 0px 10vh" class="form-group">
    <form method="post" action="{{ url('/insert') }}" >
        {{ csrf_field() }}

            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <div class="alert alert-danger">
                        {{$error}}
                    </div>
                @endforeach
            @endif


        Title: <input type="text" name="title"><br><br>
        Description: <textarea class="form-control" placeholder="description" name="description"></textarea><br><br>
        <button type="submit" id="inputEmail" class="btn btn-primary" class="form-control">Submit</button>
        <a href="{{ url('/') }}" class="btn btn-primary">Back</a>
    </form>
</div>

</body>

</html>